# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.194');

# ---------------------------------------------------------------------- #
# Modify table "branch"                                                  #
# ---------------------------------------------------------------------- #
ALTER TABLE `branch` ADD `OriginalRetailerFormElement` ENUM( 'FreeText', 'Dropdown' ) NULL DEFAULT 'FreeText';


# ---------------------------------------------------------------------- #
# Add table "retailer"                                                   #
# ---------------------------------------------------------------------- #
CREATE TABLE IF NOT EXISTS `retailer`( 
										`RetailerID` int(11) NOT NULL AUTO_INCREMENT, 
										`RetailerName` varchar(100) NOT NULL, 
										`BranchID` int(11) DEFAULT NULL, 
										`CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00', 
										`EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00', 
										`Status` enum('Active','In-active') NOT NULL DEFAULT 'Active', 
										`ModifiedUserID` int(11) DEFAULT NULL, 
										`ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
										PRIMARY KEY (`RetailerID`) 
									) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `retailer` ADD INDEX (`BranchID`);


# ---------------------------------------------------------------------- #
# Modify table "status"                                                  #
# ---------------------------------------------------------------------- #
ALTER TABLE `status` ADD `StatusTAT` INT( 11 ) NULL DEFAULT '0';
ALTER TABLE `status` ADD `StatusTATType` ENUM( 'Minutes', 'Hours', 'Days' ) NULL DEFAULT 'Minutes';


# ---------------------------------------------------------------------- #
# Add table "client_manufacturer"                                        #
# ---------------------------------------------------------------------- #
CREATE TABLE IF NOT EXISTS client_manufacturer(
												`ClientManufacturerID` int(11) NOT NULL AUTO_INCREMENT, 
												`ClientID` int(11) NOT NULL, 
												`ManufacturerID` int(11) NOT NULL, 
												`CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00', 
												`EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00', 
												`Status` enum('Active','In-active') NOT NULL DEFAULT 'Active', 
												`ModifiedUserID` int(11) DEFAULT NULL, 
												`ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
												PRIMARY KEY (`ClientManufacturerID`), 
												UNIQUE KEY `TUC_client_manufacturer_1` (`ClientID`,`ManufacturerID`), 
												KEY `IDX_client_manufacturer_ManufacturerID_FK` (`ManufacturerID`), 
												KEY `IDX_client_manufacturer_ClientID_FK` (`ClientID`), 
												KEY `IDX_client_manufacturer_ModifiedUserID_FK` (`ModifiedUserID`) 
											) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.195');
