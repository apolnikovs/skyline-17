# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.196');

# ---------------------------------------------------------------------- #
# Add table "appointment"                                            #
# ---------------------------------------------------------------------- #
ALTER TABLE `appointment` ADD COLUMN `SortOrder` INT(2) NULL DEFAULT NULL AFTER `CustContactType`;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.197');
