{extends "iframes/MasterLayout.tpl"}


{block name=config}
{$Title = 'Skyline Job Booking'}
{/block}
  
{block name=scripts}
<script>
$(document).on("click", "#print-button", function() {
        $('#job-confirmation').hide();
        $('#print-panel').show();
	return false; 
    });
$(document).on("click", "#print", function() {
        var printType = $("input[name=printRadio]:radio:checked").val();
        var url = "{$_subdomain}/Job/printJobRecord/" + {$JobID} + "/" + printType + "/?pdf=1";
        $('#print-panel').hide();
        $('#job-confirmation').show();
        window.open(url, "_blank");
        window.focus();
	return false; 
    });
$(document).on("click", "#cancel", function() {
        $('#print-panel').hide();
        $('#job-confirmation').show();
	return false; 
    });
</script>
{/block}

{block name=body}
    <div id="job-confirmation">
        <div class="sectitle">Job Booking Confirmation</div>
        {if $Status eq 'OK'}  
        <p class="confirm-item">
            <span class="label">{if $_skin eq ''}Swann {/if}Job Number:</span>
            <span class="data">{$JobID}</span>
        </p>
        <p class="section-break" />
        <p class="confirm-item">
            <span class="label">Booked Date:</span>
            <span class="data">{$Job.date_booked|date_format: '%d.%m.%Y'}</span>
        </p>
        <p class="section-break" />
        <p class="confirm-item">
            <span class="label">Customer Name:</span>
            <span class="data">{$Customer.title|upper|escape:'html'} {$Customer.first_name|upper|escape:'html'} {$Customer.last_name|upper|escape:'html'}</span>
        </p>
        <p class="section-break" />
        <p class="confirm-item">
            <span class="label">Service Required:</span>
            <span class="data">{$Job.service_type|upper|escape:'html'}</span>
        </p>
        <p class="section-break" />
        <p class="confirm-item">
            <span class="label">Manufacturer:</span>
            <span class="data">{$Job.manufacturer|upper|escape:'html'}</span>
        </p>
        <p class="section-break" />
        <p class="confirm-item">
            <span class="label">Product Type:</span>
            <span class="data">{$Job.unit_type|upper|escape:'html'}</span>
        </p>
        <p class="section-break" />
        <p class="info">
            If you have any questions regarding your appointment please contact the allocated Service Centre detailed below:
        </p>
        <div class="sectitle">Allocated Service Centre</div>
        <p class="confirm-address">
            {$ASC.name|upper|escape:'html'}<br />                     
            {if !empty($ASC.address.line_1)}{$ASC.address.line_1|upper|escape:'html'}<br />{/if}
            {if !empty($ASC.address.line_2)}{$ASC.address.line_2|upper|escape:'html'}<br />{/if}
            {if !empty($ASC.address.line_3)}{$ASC.address.line_3|upper|escape:'html'}<br />{/if}
            {if !empty($ASC.address.line_4)}{$ASC.address.line_4|upper|escape:'html'} {/if}{$ASC.address.postcode}<br />
            <br />
            {$ASC.phone|upper|escape:'html'}{if $ASC.ext ne ''} Ext: {$ASC.ext|upper|escape:'html'}{/if}<br />
            <a href="mailto:{$ASC.email|escape:'html'}">{$ASC.email|escape:'html'}</a>
        </p>
        <div id="button-bar">
            {* <a id="print-button" class="form-button" style="width: 122px;" href="#">Print</a> *}
        </div>
        {else}
        <p>Sorry there was a problem with your job booking</p>
        {/if}
        <br /><br />
        {* <div id="button-bar"><a class="form-button" href="{$_subdomain}/IFrame/JobBookingForm">Finish</a></div>*}
    </div>
            
    <div id="print-panel" class="job-booking-response" style="display: none;">
        <h1>Booking Confirmation</h1>
        <fieldset>
            <legend align='center'>Print Options</legend>
            <br />
            <input name='printRadio' type='radio' value ='2' checked /> Print Customer Receipt
            <br />
            <input name='printRadio' type='radio' value ='1' /> Print Job Card
            <br />
            <input name='printRadio' type='radio' value ='3' /> Print Service Report
            <br />
            <br />
            <div style='text-align:center;'>
                <a href='#' id='print' style='width:50px;' class='form-button'>Print</a>&nbsp;
                <a href='#' id='cancel' style='width:50px;' class='form-button'>Cancel</a>
            </div>
        </fieldset>
        <br /><br />
    </div>
{/block}