{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $PostcodeAllocations}
{/block}
{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script> 
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
    <style type="text/css" >
    .ui-combobox-input {
        width:300px;
    }
    </style>
{/block}
{block name=scripts}



    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}
    
    
    


    <script type="text/javascript">
    
     var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
     
                 
    
                    
        
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
    }
    
    
    
   
   


    $(document).ready(function() {
        $("#nId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/JobAllocation/postcodeAllocations/'+urlencode($("#nId").val()));
            }
        });
        $("#mId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/JobAllocation/postcodeAllocations/'+urlencode($("#nId").val())+'/'+urlencode($("#mId").val()));
            }
        });

                  //Click handler for finish button.
                    $(document).on('click', '#finish_btn', 
                                function() {

                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/JobAllocation');

                                });

                                      
                                      
                      {if $nId eq ''} 
                          
                          $("#mId").attr("disabled","disabled"); 
                      {/if}

                      

                     /* Add a change handler to the network dropdown - strats here*/
                        /*$(document).on('change', '#nId', 
                            function() {
                                
                                $(location).attr('href', '{$_subdomain}/JobAllocation/postcodeAllocations/'+urlencode($("#nId").val())); 
                            }      
                        );*/
                      /* Add a change handler to the network dropdown - ends here*/
                      
                        /* Add a change handler to the manufactrer dropdown - strats here*/
                        /*$(document).on('change', '#mId', 
                            function() {
                                
                                $(location).attr('href', '{$_subdomain}/JobAllocation/postcodeAllocations/'+urlencode($("#nId").val())+'/'+urlencode($("#mId").val())); 
                            }      
                        );*/
                       /* Add a change handler to the manufactrer dropdown - ends here*/
                     
              
                    /* =======================================================
                    *
                    * set tab on return for input elements with form submit on auto-submit class...
                    *
                    * ======================================================= */

                    $('input[type=text],input[type=password]').keypress( function( e ) {
                            if (e.which == 13) {
                                $(this).blur();
                                if ($(this).hasClass('auto-submit')) {
                                    $('.auto-hint').each(function() {
                                        $this = $(this);
                                        if ($this.val() == $this.attr('title')) {
                                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                            if ($this.hasClass('auto-pwd')) {
                                                $this.prop('type','password');
                                            }
                                        }
                                    } );
                                    $(this).get(0).form.onsubmit();
                                } else {
                                    $next = $(this).attr('tabIndex') + 1;
                                    $('[tabIndex="'+$next+'"]').focus();
                                }
                                return false;
                            }
                        } );        

                    var  displayButtons = "UA";
                           
                    $columnsList    = [ 
                                                /* PostcodeAllocationID */  {  "bVisible":    false },    
                                                /* Service Centre */   null,
                                                /* Repair Skill */   null,
                                                /* Client */   null,
                                                /* Job type */   null,
                                                /* Service Type */   null,
                                                /* Out of Area */  null,
                                                /* No. of Postcode areas covered */  null
                                                
                                        ];
                          
                    $('#PostcodeAllocationsResults').PCCSDataTable( {
                            aoColumns: $columnsList,
                            displayButtons:  displayButtons,
                            addButtonId:     'addButtonId',
                            addButtonText:   '{$page['Buttons']['insert']|escape:'html'}',
                            createFormTitle: '{$page['Text']['insert_page_legend']|escape:'html'}',
                            createAppUrl:    '{$_subdomain}/JobAllocation/postcodeAllocations/insert/'+urlencode($("#nId").val())+'/'+urlencode($("#mId").val())+"/",
                            createDataUrl:   '{$_subdomain}/JobAllocation/ProcessData/PostcodeAllocations/',
                            createFormFocusElementId:'NetworkID',
                            formInsertButton:'insert_save_btn',
                            copyInsertRowId: 'copyRowId',
                            
                            frmErrorRules:   {
                                                    
                                                    NetworkID:
                                                        {
                                                            required: true
                                                        },
                                                    RepairSkillID:
                                                        {
                                                            required: true
                                                        },
                                                    ManufacturerID:
                                                        {
                                                            required: true
                                                        }    
                                                        
                                                        
                                             },
                                                
                           frmErrorMessages: {
                                                
                                                    NetworkID:
                                                        {
                                                            required: "{$page['Errors']['network']|escape:'html'}"
                                                        },
                                                    RepairSkillID:
                                                        {
                                                             required: "{$page['Errors']['repair_skill']|escape:'html'}"
                                                        },
                                                    ManufacturerID:
                                                        {
                                                             required: "{$page['Errors']['manufacturer']|escape:'html'}"
                                                        }    
                                                     
                                              },                     
                            
                            popUpFormWidth:  750,
                            popUpFormHeight: 430,
                            updateButtonId:  'updateButtonId',
                            updateButtonText:'{$page['Buttons']['view']|escape:'html'}',
                            updateFormTitle: '{$page['Text']['update_page_legend']|escape:'html'}',
                            updateAppUrl:    '{$_subdomain}/JobAllocation/postcodeAllocations/view/',
                            colorboxFormId:  "PostcodeAllocationsForm",
                            frmErrorMsgClass:"fieldError",
                            frmErrorElement: "label",
                            htmlTablePageId: 'PostcodeAllocationsResultsPanel',
                            htmlTableId:     'PostcodeAllocationsResults',
                            fetchDataUrl:    '{$_subdomain}/JobAllocation/ProcessData/PostcodeAllocations/fetch/'+urlencode("{$nId}")+"/"+urlencode("{$mId}"),
                            formCancelButton:'cancel_btn',
                            dblclickCallbackMethod: 'gotoEditPage',
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
                            iDisplayLength:  25,
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError",
                            sDom: 'ft<"#dataTables_command">rpli',
                            bottomButtonsDivId:'dataTables_command',
                            displayProcessingTextId:'display_process_text_id',
                            displayProcessingText:"{$page['Text']['processing_wait']|escape:'html'}",
			    aaSorting: [[1, "asc"]]
                        });
                      

                        
                   

    });

    </script>

    {/block}

    {block name=body}

    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/jobAllocation" >{$page['Text']['job_allocation']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" >

               <div class="ServiceAdminTopPanel" >
                    <form id="PostcodeAllocationsTopForm" name="PostcodeAllocationsTopForm" method="post"  action="#" class="inline" >

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                <div class="ServiceAdminResultsPanel" id="PostcodeAllocationsResultsPanel" >
                    <form id="nIdForm" class="nidCorrections">
                        {if $SupderAdmin eq true} 
                            {$page['Labels']['network_label']|escape:'html'}
                            <select name="nId" class="topDropDownList" id="nId" >
                                <option value="" {if $nId eq ''}selected="selected"{/if}>{$page['Text']['select_service_network']|escape:'html'}</option>

                                {foreach $networks as $network}

                                    <option value="{$network.NetworkID}" {if $nId eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                                {/foreach}
                            </select>
                            &nbsp;&nbsp;  

                        {else}

                            <input type="hidden" name="nId" id="nId" value="{$nId|escape:'html'}" >

                        {/if} 
                        <br /><span style="padding-right: 15px;">{$page['Labels']['manufacturer_label']|escape:'html'}</span>
                        <select name="mId" class="topDropDownList" id="mId" >
                            <option value="" {if $mId eq ''}selected="selected"{/if}>{$page['Text']['select_manufacturer']|escape:'html'}</option>

                            {foreach $manufacturers as $manufacturer}

                                <option value="{$manufacturer.ManufacturerID}" {if $mId eq $manufacturer.ManufacturerID}selected="selected"{/if}>{$manufacturer.ManufacturerName|escape:'html'}</option>

                            {/foreach}
                        </select> 
                         
                        
                     </form>
                    
                        
                        
                    <form id="PostcodeAllocationsResultsForm" class="dataTableCorrections">
                        <table id="PostcodeAllocationsResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                            <thead>
                                    <tr>
                                            <th></th> 
                                            <th title="{$page['Text']['service_centre']|escape:'html'}" >{$page['Text']['service_centre']|escape:'html'}</th>
                                            <th title="{$page['Text']['repair_skill']|escape:'html'}" >{$page['Text']['repair_skill']|escape:'html'}</th>
                                            <th title="{$page['Text']['client']|escape:'html'}" >{$page['Text']['client']|escape:'html'}</th>
                                            <th title="{$page['Text']['job_type']|escape:'html'}" >{$page['Text']['job_type']|escape:'html'}</th>
                                            <th title="{$page['Text']['service_type']|escape:'html'}" >{$page['Text']['service_type']|escape:'html'}</th>
                                            <th title="{$page['Text']['ooa']|escape:'html'}" >{$page['Text']['ooa']|escape:'html'}</th>
                                            <th title="{$page['Text']['postcodes_covered']|escape:'html'}" >{$page['Text']['postcodes_covered']|escape:'html'}</th>
                                            
                                    </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>  
                        
                     </form>
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                    
                </div>        
                 <div class="centerInfoText" id="centerInfoText" style="display:none;" ></div>
               <input type="hidden" name="copyRowId" id="copyRowId" > 
                 
    </div>
                        
                        



{/block}



