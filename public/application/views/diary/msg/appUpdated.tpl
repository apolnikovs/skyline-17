

{block name=config}
{$Title = "Skyline Diary FAQ"}
{$PageId = 89}

    


{/block}
{block name=afterJqueryUI}


{/block}

{block name=scripts}

      <script type="text/javascript" src="{$_subdomain}/js/jquery-1.8.2.min.js"></script>
<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />

	<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>

       




<script>
  
       $(document).ready(function() {
       $.colorbox({ html:$('#msgDiv').html(), title:"",escKey: false,
    overlayClose: false}

);
   $('#cboxClose').remove();
       });
       
       
</script>
{/block}




{block name=body}


    {if $mode=='u'}
    <div id="msgDiv" style="display:none">
    
        <div style="width:400px;text-align: center;">Thank you for confirming receipt of this appointment, your confirmation has been acknowledged by our systems.</div>
        </div>
        {elseif $mode=='n'}
             <div id="msgDiv" style="display:none">
    
      <div style="width:400px;text-align: center;"> Thank you for confirming receipt of this appointment, however the confirmation has already been acknowledged.</div>
       </div> 
             {else}
                 <div id="msgDiv" style="display:none">
    
      <div style="width:400px;text-align: center;"> The appointment you are attempting to confirm has been cancelled, no further action is required.</div>
       </div> 
        {/if}
        
{/block}
